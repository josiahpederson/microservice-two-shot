from email.quoprimime import body_check
import re
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import Hat, LocationVO
import json
from django.http import JsonResponse
from common.json import ModelEncoder


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["shelf_number", "section_number", "closet_name", "id_number", "import_href", "id"]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["style", "color", "id"]


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = ["style", "color", "id", "fabric", "picture", "location"]

    encoders = {
        "location": LocationVODetailEncoder(),
    }

# Create your views here.

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder
        )
    else:
        content = json.loads(request.body)

        try:
            location = LocationVO.objects.get(id_number=content["location"])
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Location id invalid"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )

@require_http_methods(["PUT", "DELETE", "GET"])
def api_detail_hat(request, pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )

@require_http_methods(["GET"])
def api_list_locations(request):
    locations = LocationVO.objects.all()
    return JsonResponse(
        {"locations": locations},
        encoder=LocationVODetailEncoder,
        safe=False,
    )