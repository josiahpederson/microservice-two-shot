# Generated by Django 4.0.3 on 2022-06-15 20:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='LocationVO',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('shelf_number', models.SmallIntegerField()),
                ('section_number', models.SmallIntegerField()),
                ('closet_name', models.CharField(max_length=40)),
                ('id_number', models.SmallIntegerField(unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Hat',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fabric', models.CharField(max_length=25)),
                ('style', models.CharField(max_length=30)),
                ('color', models.CharField(max_length=20)),
                ('picture', models.URLField(blank=True, null=True)),
                ('location', models.OneToOneField(on_delete=django.db.models.deletion.PROTECT, related_name='hats', to='hats_rest.locationvo')),
            ],
        ),
    ]
