from django.urls import path

from .views import api_list_hats, api_detail_hat, api_list_locations

urlpatterns = [
    path("hats/", api_list_hats, name="api_list_hats"),
    path("hats/<int:pk>/", api_detail_hat, name="api_detail_hat"),
    path("locations/", api_list_locations, name="api_list_locations"),
]