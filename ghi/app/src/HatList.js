import React from 'react';
import { Link } from 'react-router-dom';


async function DeleteHat(id) {
    const url = `http://localhost:8090/api/hats/${id}/`
    const fetchConfig = {
        method: "DELETE",
    }
    const response = await fetch(url, fetchConfig)
    if (response.ok) {
        window.location.reload(false)
    } else {
        console.error(response);
    }
}


function HatColumn(props) {
    return (
        <div className="col">
            {props.list.map(data => {
                const hat = data;
                return (
                    <div key={hat.id} className="card mb-3 shadow">
                        {/* <img src={hat.picture} className="card-img-top" /> */}
                        <div className="card-body">
                            <h5 className="card-title">{hat.color} {hat.fabric} {hat.style} hat</h5>
                            <p className="card-text">
                                Located in: {hat.location.closet_name} closet on shelf #{hat.location.shelf_number} in section #{hat.location.section_number}
                            </p>
                            <button type="button" className="btn btn-outline-danger mx-3" onClick={() => DeleteHat(hat.id)}>Delete</button>
                            <button type="button" className="btn btn-outline-primary"onClick={(e) => {
                                e.preventDefault();
                                window.location.href=hat.picture;
                            }}
                            >See Image</button>
                        </div>
                    </div>
                );
            })}
      </div>        
    )
}


class HatList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            hatColumns: [[], [], []],
        };
    }

    async componentDidMount() {
        const url = "http://localhost:8090/api/hats/";
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();

                const requests = []

                for (let hat of data.hats) {
                    const detailUrl = `http://localhost:8090/api/hats/${hat.id}/`
                    requests.push(fetch(detailUrl));
                }

                const responses = await Promise.all(requests);

                const hatColumns = [[], [], []];

                let i = 0
                for (const hatResponse of responses) {
                    if (hatResponse.ok) {
                        const details = await hatResponse.json();
                        hatColumns[i].push(details);
                        i = i + 1;
                        if (i > 2) {
                            i = 0;
                        }
                    } else {
                        console.error(hatResponse)
                    }
                }
                this.setState({hatColumns: hatColumns})
            }
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        return (
            <div className="container">
                <div className="d-flex justify-content-center pt-2">
                    <h2>Hat List</h2>
                </div>
                <div className="d-flex justify-content-center pb-4">
                    <Link to="/hats/new" className="btn btn-outline-primary d-flex justify-content-center">Create New Hat!</Link>
                </div>
                <div className="row">
                {this.state.hatColumns.map((hatList, index) => {
                    return (
                    <HatColumn key={index} list={hatList} />
                    );
                })}
                </div>
          </div>
        )
    }
}

export default HatList;